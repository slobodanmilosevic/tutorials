﻿using Commander.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Commander.Data
{
    public class MockCommanderRepo : ICommanderRepo
    {
        public IEnumerable<Command> GetAppCommands()
        {
            var commnads = new List<Command>
            {
                new Command { Id = 0, HowTo = "Boil an egg", Line = "Boil water", Platform = "Smederevac" },
                new Command { Id = 0, HowTo = "Skuvaj jaje", Line = "Zagrej vodu", Platform = "Alfa" },
                new Command { Id = 0, HowTo = "Isprzi jaja", Line = "Zagrej ulje", Platform = "Parno" }
            };
            return commnads;
        }

        public Command GetCommandById(int id)
        {
            return new Command { Id = 0, HowTo = "Boil an egg", Line = "Boil water", Platform = "Smederevac" };
        }
    }
}

